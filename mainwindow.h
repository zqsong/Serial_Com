#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "win_qextserialport.h"
#include    <QMainWindow>
#include    <QDir>
#include    <QFile>
//#include    <QFileInfo>
#include    <QTextStream>
#include    <QTextBrowser>
//#include    <time.h>
//#include    <QDebug>
#include    <QSettings>
#include    <QTextCodec>
#include    <QDateTime>
#include    <QFileDialog>
#include    <QMessageBox>
namespace Ui {
    class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Win_QextSerialPort *myCom;     //串口

    mutable QByteArray temp;
    mutable int buffersize;
    mutable int read_count;
    mutable int data[32];

    void data_display();
    void Init_Port();
    QFile *file;


private slots:

    void on_Openbtn_clicked();
    void Read_Port();
    void ReadSettings(QString buf);
    void on_openFile_clicked();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
