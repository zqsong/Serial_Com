#-------------------------------------------------
#
# Project created by QtCreator 2015-07-02T23:14:06
#
#-------------------------------------------------

QT       += core gui widgets

TARGET = COM_receive
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    win_qextserialport.cpp \
    qextserialbase.cpp

HEADERS  += mainwindow.h \
    win_qextserialport.h \
    qextserialbase.h

FORMS    += mainwindow.ui

OTHER_FILES += \
    my.rc

RC_FILE = \
my.rc
